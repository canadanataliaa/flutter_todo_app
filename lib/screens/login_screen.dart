import 'dart:async';

import 'package:flutter/material.dart';

import 'package:email_validator/email_validator.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '/models/user.dart';
import '/providers/user_provider.dart';
import '/utils/api.dart';
import '/utils/functions.dart';

class LoginScreen extends StatefulWidget {
    @override
    _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
    Future<User>? _futureLogin;
    
    final _formKey = GlobalKey<FormState>();
    final _tffEmailController = TextEditingController();
    final _tffPasswordController = TextEditingController();

    void login(BuildContext context) {
        setState(() {
            _futureLogin = API().login(
                email: _tffEmailController.text, 
                password: _tffPasswordController.text
            ).catchError((error) {
                showSnackBar(context, error.message);
            });
        });
    }

    void setSharedPref(response, Function setUserId, Function setAccessToken) {
        Timer(Duration(milliseconds: 1), () async {
            SharedPreferences prefs = await SharedPreferences.getInstance();
            User user = response.data as User;
            
            setUserId(user.id);
            setAccessToken(user.accessToken);

            // The '!' means that if the user.accessToken is null, it will do nothing. Else it will give the value.
            prefs.setString('accessToken', user.accessToken!);  
            prefs.setInt('userId', user.id!);  

            Navigator.pushReplacementNamed(context, '/task-list');
        });
    }

    @override
    Widget build(BuildContext context) {
        final Function setAccessToken = Provider.of<UserProvider>(context, listen: false).setAccessToken;
        final Function setUserId = Provider.of<UserProvider>(context, listen: false).setUserId;

        Widget tffEmail = TextFormField(
            decoration: InputDecoration(labelText: 'Email'),
            keyboardType: TextInputType.emailAddress,
            controller: _tffEmailController,
            validator: (email) {
                if (email == null || email.isEmpty) {
                    return 'The email must be provided.';
                } else if (EmailValidator.validate(email) == false) {
                    return 'A valid email must be provided.';
                }

                return null;
            }
        ); 

        Widget tffPassword = TextFormField(
            decoration: InputDecoration(labelText: 'Password'),
            obscureText: true,
            controller: _tffPasswordController,
            validator: (password) {
                bool isPasswordValid = password != null && password.isNotEmpty;
                return isPasswordValid ? null : 'The password must be provided.';
            }
        ); 

        Widget btnSubmit = Container(
            width: double.infinity,
            margin: EdgeInsets.only(top: 8.0),
            child: ElevatedButton(
                child: Text('Login'),
                onPressed: () {
                    if (_formKey.currentState!.validate()) {
                        login(context);
                    } else {
                        print('The login form is not valid.');
                    }
                }
            )
        );

        Widget btnGoToRegister = Container(
            width: double.infinity,
            child: ElevatedButton(
                child: Text('No account yet?'),
                onPressed: () {
                    Navigator.pushNamed(context, '/register');
                }
            )
        );

        Widget formLogin = Form(
            key: _formKey,
            child: Column(
                children: [
                    tffEmail,
                    tffPassword,
                    btnSubmit,
                    btnGoToRegister
                ]
            )
        );

        Widget loginView = FutureBuilder(
            future: _futureLogin,
            builder: (context, response) {
                if (_futureLogin == null) {
                    return formLogin;
                } else if (response.hasError == true) {
                    return formLogin;
                } else if (response.hasData == true) {
                    setSharedPref(response, setUserId, setAccessToken);
                }

                return Center(
                        child: CircularProgressIndicator()
                );
            }
        );

        return Scaffold(
            appBar: AppBar(title: Text('Todo Login')),
            body: Container (
                width: double.infinity,
                padding: EdgeInsets.all(16.0),
                child: loginView,
            ),
        );
  }
}