import 'package:flutter/material.dart';

import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

import '/models/task.dart';
import '/providers/user_provider.dart';
import '/utils/api.dart';
import '/utils/functions.dart';

class TaskDetailScreen extends StatefulWidget {
    final Task _task;

    TaskDetailScreen(this._task);

    @override
    TaskDetailScreenState createState() => TaskDetailScreenState();
}

class TaskDetailScreenState extends State<TaskDetailScreen> {
    Future<Task>? _futureImageUpload;

    void uploadImage(String accessToken) async {
        var selectedImage = await ImagePicker().pickImage(source: ImageSource.gallery);

        setState(() {
            if (selectedImage != null) {
                _futureImageUpload = API(accessToken).addTaskImage(
                    id: widget._task.id, 
                    filePath: selectedImage.path
                ).catchError((error) {
                    showSnackBar(context, error.message);
                });
            }
        });
    }

    @override
    Widget build(BuildContext context) {
        final String? accessToken = Provider.of<UserProvider>(context).accessToken;

        Widget btnUpload = Container(
            width: double.infinity,
            padding: EdgeInsets.only(top: 16.0),
            child: ElevatedButton(
                child: Text('Upload Image'),
                onPressed: () => uploadImage(accessToken as String)
            )
        );

        Widget imageView = FutureBuilder(
            future: _futureImageUpload,
            builder: (context, snapshot) {
                return Container();
            }
        );

        return Scaffold(
            appBar: AppBar(title: Text('Task Detail')),
            body: Container(
                width: double.infinity,
                padding: EdgeInsets.all(16.0),
                child: SingleChildScrollView(
                    child: Column(
                        children: [
                            Text(widget._task.description),
                            btnUpload,
                            imageView
                        ]
                    )
                )
            )
        );
    }
}