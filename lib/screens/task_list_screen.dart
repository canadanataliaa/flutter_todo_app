import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '/models/task.dart';
import '/providers/user_provider.dart';
import '/utils/api.dart';
import '/widgets/add_task_dialog.dart';
import '/widgets/task_item_tile.dart';
import '/utils/functions.dart';

class TaskListScreen extends StatefulWidget {
    @override
    TaskListScreenState createState() => TaskListScreenState();
}

class TaskListScreenState extends State<TaskListScreen> {
    Future<List<Task>>? _futureTasks;

    void reloadTasks() {
        final String? accessToken = Provider.of<UserProvider>(context, listen: false).accessToken;

        setState(() {
            _futureTasks = API(accessToken).getTasks().catchError((error) {
                showSnackBar(context, error.message);
            });
        });
    }
    
    void showAddTaskDialog(BuildContext context) {
        showDialog(
            context: context,
            builder: (BuildContext context) => AddTaskDialog()
        ).then((value) {
            reloadTasks();
        });
    }

    void removeSharedPref() async {
        Provider.of<UserProvider>(context, listen: false).setAccessToken(null);
        Provider.of<UserProvider>(context, listen: false).setUserId(null);

        SharedPreferences prefs = await SharedPreferences.getInstance();

        prefs.remove('accessToken');
        prefs.remove('userId');

        Navigator.pushNamedAndRemoveUntil(context, '/', (route) => false);
    }

    Widget showTasks(List? tasks) {
        var cltTasks = tasks!.map((task) => TaskItemTile(task)).toList();
        
        return ListView(
            children: cltTasks
        );
    }
    
    @override
    void initState() {
        super.initState();

        WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
            reloadTasks();
        });
    }

    @override
    Widget build(BuildContext context) {
        Widget taskListView = FutureBuilder(
            future: _futureTasks,
            builder: (context, response) {
                if (response.hasData) {
                    return showTasks(response.data as List);
                }
                
                return Center(
                    child: CircularProgressIndicator()
                );
            }
        );

        return Scaffold(
            appBar: AppBar(title: Text('Todo List')),
            drawer: Drawer(
                child: ListView(
                    children: [
                        ListTile(
                            title: Text('Logout'),
                            onTap: () => removeSharedPref()
                        )
                    ]
                )
            ),
            body: Container (
                width: double.infinity,
                padding: EdgeInsets.all(16.0),
                child: taskListView,
            ),
            floatingActionButton: FloatingActionButton(
                child: Icon(Icons.add),
                backgroundColor: Color.fromRGBO(255, 212, 71, 1),
                foregroundColor: Colors.black,
                onPressed: () {
                    showAddTaskDialog(context);
                }
            )
        );
    }
}